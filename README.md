# fr.tezosmtl.com

> Ce dépôt contient les fichiers source de [fr.tezosmtl.com](https://fr.tezosmtl.com/).

## Contribution

En contribuant à ce site web, vous dédiez votre travail au domaine public et renoncez à toute revendication de droit d'auteur en vertu des conditions du [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).

### Soumettre une pull request

- [Forkez ce dépôt GitLab](https://gitlab.com/tezosmtl/tezosmtl.com/forks/new) et clonez votre fork localement sur votre ordinateur.
- Installez [Jekyll](https://jekyllrb.com/) et autres dépendances avec la commande `make install`*
- Testez vos changements avec la commande `make serve`
- Une fois satisfait, poussez vos modifications et ouvrez une pull request

\* Assurez-vous d'avoir [Bundler](https://bundler.io/) installé.

Si vous n'êtes pas en mesure d'ouvrir une pull request, n'hésitez pas à [ouvrir une issue](https://gitlab.com/tezosmtl/tezosmtl.com/issues/new) à la place.

## Crédits

Ce site web est basé sur un thème [Jekyll](https://jekyllrb.com/) nommé [Jasper2](https://github.com/jekyller/jasper2) (qui est lui-même une version du thème [Casper](https://github.com/TryGhost/Casper) de Ghost).

## Domaine public

Sauf indication contraire, tout le contenu de ce site web est [dédié au domaine public](https://gitlab.com/tezosmtl/fr.tezosmtl.com/blob/master/LICENSE) sous la [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).

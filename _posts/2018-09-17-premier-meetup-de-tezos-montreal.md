---
layout: post
current: post
cover: assets/images/marc-olivier-jodoin-118293-unsplash.jpg
navigation: True
title: Premier meetup de Tezos Montréal
excerpt: Pour célébrer le lancement du mainnet de Tezos, nous organisons notre premier meetup ce mercredi 19 septembre.
date: 2018-09-17 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_en: /tezos-montreal-first-meetup/
---

### Quand ?

Mercredi 19 septembre de 6:00 PM à 9:00 PM

### Où ?

Bureau de [YAP.cx](https://yap.cx/), 201 rue Notre-Dame Ouest, bureau 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

Nous ([Francis Brunelle](https://twitter.com/frabrunelle) et [Zakaria Boukhcheb](https://twitter.com/zakaria_mb)) sommes heureux d'annoncer la création du groupe meetup Tezos Montréal !

Ce groupe meetup s'adresse à tout le monde qui souhaite en apprendre davantage sur [Tezos](https://tezos.com/) et rencontrer d'autres passionnés de Tezos.

Pour célébrer le [lancement du mainnet de Tezos](https://tezos.foundation/news/tezos-mainnet-is-live), nous organisons notre premier meetup ce mercredi 19 septembre.

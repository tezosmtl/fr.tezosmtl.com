---
layout: post
current: post
cover: assets/images/andrew-welch-40121-unsplash.jpg
navigation: True
title: Une introduction au proof-of-stake
excerpt: Nous organisons un meetup sur le proof-of-stake au BlockHouse le samedi 20 octobre.
date: 2018-10-02 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_en: /an-introduction-to-proof-of-stake/
---

### Quand ?

Samedi 20 octobre de 2:00 PM à 5:00 PM

### Où ?

[BlockHouse](https://blockhouse.rcgt.com/fr/), 456 rue De La Gauchetière O, bureau 200 ([map](https://goo.gl/maps/Aj6Zn9FHBHS2))

### Description

Emmanuel Rochette de [Catallaxy](https://catallaxy.rcgt.com/fr/) présentera un aperçu du proof-of-stake. La discussion débutera avec une brève comparaison entre le proof-of-work (PoW) et le proof-of-stake (PoS) pour expliquer ce qui a poussé au développement de ce dernier. Elle portera ensuite sur l’évolution du PoS à travers son usage dans diverses cryptomonnaies. La discussion se terminera sur les attaques qui menacent les systèmes utilisant le PoS et leurs solutions correspondantes.

[Zakaria Boukhcheb](https://twitter.com/zakaria_mb) parlera du proof-of-stake dans le contexte de la blockchain Tezos (e.g. expliquant le fonctionnement du baking) et comparera l'implémentation du proof-of-stake dans Tezos à une autre variante populaire (contrastant le [liquid proof-of-stake](https://medium.com/tezos/liquid-proof-of-stake-aec2f7ef1da7) et le delegated proof-of-stake). Il abordera également d'autres aspects de Tezos (histoire, philosophie, gouvernance et amendements potentiels).

[Samuel Harrison](https://twitter.com/XTZAccelerated) de la [Fondation Tezos Commons](https://tezoscommons.org/) donnera une présentation sur la communauté et l’écosystème de Tezos.

La présentation de Emmanuel ainsi que celle de Zakaria seront en français. La présentation de Samuel sera en anglais.

### Diapositives

Voici les diapositives de la présentation de Zakaria :

- [Présentation Tezos](https://docs.google.com/presentation/d/1DpJulwkSdTMf_0qaTYPwcpsM5iJRZyhRDPc4ODCUgQg/edit?usp=sharing)

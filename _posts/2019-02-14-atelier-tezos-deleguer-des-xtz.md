---
layout: post
current: post
cover: assets/images/annie-spratt-608001-unsplash.jpg
navigation: True
title: 'Atelier Tezos : déléguer des XTZ'
excerpt: Nous organisons un atelier Tezos au bureau de YAP.cx le mercredi 20 février.
date: 2019-02-14 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_en: /tezos-workshop-how-to-delegate-xtz/
---

### Quand ?

Mercredi 20 février de 6:45 PM à 9:00 PM

### Où ?

Bureau de [YAP.cx](https://yap.cx/), 201 rue Notre-Dame Ouest, bureau 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

Cet atelier sera une opportunité pour tester différents portefeuilles Tezos présentement disponibles. Nous allons faire une démonstration de [TezBox](https://tezbox.com), [Galleon](https://galleon-wallet.tech) et [Cortez](https://play.google.com/store/apps/details?id=com.tezcore.cortez). Nous vous encourageons à amener un laptop si vous souhaitez suivre les démonstrations pendant l'atelier. Nous utiliserons des jetons test (des réseaux Alphanet et Zeronet), alors tout le monde sera en mesure de participer !

Nous allons également présenter différents outils disponibles pour choisir un baker tels que [MyTezosBaker](https://mytezosbaker.com/), ainsi que [Baking Bad](https://baking-bad.org/) pour auditer le baker une fois choisi.

Nous planifions effectuer l'atelier en français, mais nous pourrons également répéter les explications et répondre aux questions en anglais, si les participants le souhaitent.

### Diapositives

Voici les diapositives de nos présentations :

- [Atelier Tezos : déléguer des XTZ](https://docs.google.com/presentation/d/1WSS77lJCSbbsLjE3S33tIV4z_4kj6mH69MSYlbkzk4w/edit?usp=sharing)
- [Comment choisir un baker](https://docs.google.com/presentation/d/1VBphbmcz1_GVijFdyoeg1lmewUs2Q0w2VDazmZO-3K4/edit?usp=sharing)

N'hésitez pas à les utiliser comme modèles pour préparer une présentation sur Tezos ou tout simplement comme guide étape par étape à la délégation.

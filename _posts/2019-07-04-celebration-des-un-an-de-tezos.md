---
layout: post
current: post
cover: assets/images/ray-hennessy-gdTxVSAE5sk-unsplash.jpg
navigation: True
title: Célébration des un an de Tezos !
excerpt: Pour célébrer le premier anniversaire du lancement du réseau Tezos, nous organisons un meetup le mercredi 10 juillet.
date: 2019-07-04 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_en: /tezos-one-year-celebration/
---

### Quand ?

Mercredi 10 juillet de 5:00 PM à 7:00 PM

### Où ?

Bureau de [YAP.cx](https://yap.cx/), 201 rue Notre-Dame Ouest, bureau 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

[Pour célébrer le premier anniversaire du lancement du réseau Tezos](https://medium.com/tezoscommons/tezos-celebrating-one-year-da53b98a5084), nous organisons un meetup le mercredi 10 juillet.

Zakaria Boukhcheb donnera une brève introduction à Tezos et au liquid proof-of-stake (15 à 20 minutes, en français). Le reste du meetup sera principalement du réseautage.

Merci à [Tezos Commons](https://tezoscommons.org/) de supporter ce meetup !

### RSVP

- [Decentralized Web Montreal](https://www.meetup.com/decentralized-web-montreal/events/262874990/)

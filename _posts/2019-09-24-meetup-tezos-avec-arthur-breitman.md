---
layout: post
current: post
cover: assets/images/eva-blue-1xgnRBvF_UI-unsplash.jpg
navigation: True
title: Meetup Tezos avec Arthur Breitman
excerpt: Nous organisons un meetup Tezos au bureau de YAP.cx ce jeudi 26 septembre.
date: 2019-09-24 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_en: /tezos-meetup-with-arthur-breitman/
---

### Quand ?

Jeudi 26 septembre de 6:30 PM à 8:30 PM

### Où ?

Bureau de [YAP.cx](https://yap.cx/), 201 rue Notre-Dame Ouest, bureau 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

Nous organisons un meetup Tezos au bureau de YAP.cx ce jeudi 26 septembre. Ce sera une excellente occasion de rencontrer d'autres passionnés de Tezos et de discuter avec eux. Si vous êtes nouveau à Tezos, vous êtes aussi les bienvenus à ce meetup, nous serons heureux de répondre à vos questions !

[Arthur Breitman](https://twitter.com/arthurb) sera présent lors du meetup et fera une session de questions-réponses et une courte présentation.

![Photo Arthur](/assets/images/arthur.jpg)

Arthur Breitman est l'un des premiers architectes du protocole Tezos. Avant de s'impliquer à temps plein dans Tezos, il a travaillé chez X et Waymo sur des voitures autonomes. Au début de sa carrière, il a travaillé comme analyste quantitatif chez Goldman Sachs et Morgan Stanley. Arthur est diplômé de l'École polytechnique et est titulaire d'une maîtrise en mathématiques financières de l'Institut Courant de NYU.

### RSVP

- [Decentralized Web Montreal](https://www.meetup.com/decentralized-web-montreal/events/265119742/)

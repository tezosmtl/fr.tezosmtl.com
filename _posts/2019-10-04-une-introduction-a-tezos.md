---
layout: post
current: post
cover: assets/images/july-10-meetup.jpg
navigation: True
title: Une introduction à Tezos
date: 2019-10-04 12:00:00
tags: [Videos]
class: post-template
subclass: 'post tag-videos'
author: francis
permalink_en: /an-introduction-to-tezos/
---

Voici une vidéo de la [présentation Tezos](https://docs.google.com/presentation/d/11vQh0UpZx-zBvp4jYqofTu9yahCjHa4YbfyjLUdJsSA/edit?usp=sharing) donnée par Zakaria lors de notre [meetup du 10 juillet](/celebration-des-un-an-de-tezos/).

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/mxcUDiSln7Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Nous espérons que ce sera une ressource utile pour la communauté francophone de Tezos !

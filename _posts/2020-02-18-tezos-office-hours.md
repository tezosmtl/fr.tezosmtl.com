---
layout: post
current: post
cover: assets/images/matthew-fournier-m98qq5300rk-unsplash.jpg
navigation: True
title: Tezos Office Hours
excerpt: Nous organisons un meetup Tezos au bureau de YAP.cx le mercredi 26 février.
date: 2020-02-17 12:00:00
tags: [Meetups]
class: post-template
subclass: 'post tag-meetups'
author: francis
permalink_en: /tezos-office-hours/
---

### Quand ?

Mercredi 26 février de 6:30 PM à 9:30 PM

### Où ?

Bureau de [YAP.cx](https://yap.cx/), 201 rue Notre-Dame Ouest, bureau 700 ([map](https://goo.gl/maps/mHx33NnUxz42))

### Description

Inspirés par le récent meetup [Tezos Happy Office Hours](https://www.eventbrite.com/e/tezos-happy-office-hours-tickets-92216218191), nous organisons un meetup Tezos pour réseauter et discuter des derniers développements reliés à Tezos.

Êtes-vous un développeur sur la plateforme Tezos ou bien simplement curieux ? Rejoignez-nous au bureau de Yap.cx pour rencontrer et discuter avec des membres de la communauté Tezos à Montréal.

[Simon B.Robert](https://github.com/carte7000), développeur principal de [Taquito](https://tezostaquito.io/) (une librairie TypeScript pour développer sur la blockchain Tezos) sera aussi présent. Nous (Francis, Zak et Simon) serons sur place pour répondre aux questions et guider les nouveaux utilisateurs, que ce soit pour un utilisateur régulier ou un développeur. Nous vous invitons à amener votre laptop si vous avez besoin d'aide concernant la création d'un wallet ou pour mettre en place un environnement de développement.

Veuillez noter qu'il n'y aura pas de présentations durant ce meetup, mais que nous planifions d'organiser un évènement au mois de Mars, durant lequel Zak et Simon feront chacun une présentation. Nous espérons que ce meetup sera une occasion de recueillir des commentaires et recommandations sur les sujets à aborder durant le prochain meetup.

Cet évènement est supporté par la [Tezos Commons Foundation](https://tezoscommons.org/).

### RSVP

- [Decentralized Web Montreal](https://www.meetup.com/decentralized-web-montreal/events/268784797/)

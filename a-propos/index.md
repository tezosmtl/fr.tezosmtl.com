---
layout: page
current: about
title: À propos de Tezos Montreal
navigation: true
class: page-template
subclass: 'post page'
permalink_en: /about/
---

Ce groupe meetup s'adresse à tout le monde qui souhaite en apprendre davantage sur [Tezos](https://tezos.com/) et rencontrer d'autres passionnés de Tezos. Il a été créé par [Francis Brunelle](https://frabrunelle.com/) et [Zakaria Boukhcheb](https://twitter.com/zakaria_mb) en septembre 2018.

Pour être averti lorsque nous annonçons de nouveaux meetups, vous pouvez [vous abonner à notre newsletter](#subscribe) ou rejoindre le groupe meetup [Decentralized Web Montreal](https://www.meetup.com/decentralized-web-montreal/).

Suivez-nous sur [Twitter](https://twitter.com/tezosmtl) et rejoingnez-nous sur [Telegram](https://t.me/tezosmtl) pour discuter avec nous et nous laisser des commentaires constructifs.

## Contribution

En contribuant à ce site web, vous dédiez votre travail au domaine public et renoncez à toute revendication de droit d'auteur en vertu des conditions du [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).

### Soumettre une pull request

- [Forkez ce dépôt GitLab](https://gitlab.com/tezosmtl/tezosmtl.com/forks/new) et clonez votre fork localement sur votre ordinateur.
- Installez [Jekyll](https://jekyllrb.com/) et autres dépendances avec la commande `make install`*
- Testez vos changements avec la commande `make serve`
- Une fois satisfait, poussez vos modifications et ouvrez une pull request

\* Assurez-vous d'avoir [Bundler](https://bundler.io/) installé.

Si vous n'êtes pas en mesure d'ouvrir une pull request, n'hésitez pas à [ouvrir une issue](https://gitlab.com/tezosmtl/tezosmtl.com/issues/new) à la place.

## Crédits

Ce site web est basé sur un thème [Jekyll](https://jekyllrb.com/) nommé [Jasper2](https://github.com/jekyller/jasper2) (qui est lui-même une version du thème [Casper](https://github.com/TryGhost/Casper) de Ghost).

## Domaine public

Sauf indication contraire, tout le contenu de ce site web est [dédié au domaine public](https://gitlab.com/tezosmtl/fr.tezosmtl.com/blob/master/LICENSE) sous la [CC0 Public Domain Dedication](https://creativecommons.org/publicdomain/zero/1.0/).
